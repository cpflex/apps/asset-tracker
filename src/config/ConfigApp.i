/**
*  Name:  ConfigApp.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2023 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/


//Specify the diversity settings used by the App.
//These use settings defined in the SimpleTracking configuration file.
const MIN_TRACK_OFFSET = 2;
const MAX_TRACK_OFFSET = DEFAULT_INTERVAL*60;