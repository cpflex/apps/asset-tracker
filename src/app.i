/**
 *  Name:  app.i
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2021 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
 
#include "ui.i"
#include "timer.i"
#include "log.i"
#include "telemetry.i"
#include "NvmRecTools.i"
#include "OcmNidDefinitions.i"
 

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch enter power saving events.
*/
forward app_EnterPowerSaving(pctCharged);

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch exit power saving events.
*/
forward app_ExitPowerSaving(pctCharged);

/***
* @brief Callback handler implemented by the application to receive
*   notification of state changes and signals.
* @param id The callback event identifier.
* @param stateCur  The array containing the current state data. Array has structure
*  as follows:  [ .idxState, .tickSamples, .tickMotion, .userValue, .tStateChange, .ctMeasured, .tLastLocate].
* @param data The array containing the data. Array has structure
*  as follows:   Rows[3] are Physical Axes (X,Y,Z) and
*  Columns are[4]:  [ Max Accel (mg), Min Accel (mg), Delta V (mgs), |Delta V (mgs)|.
*/
forward app_ImpulseSensorEvent(  
	id,
	const stateCur[ .flags, .tActive, .ctRead, .dt, .ctSamples, .maxAccel],
    const data[3][4]
);

/***
* @brief Callback handler implemented by the application to receive
*   notification of temperature sensor state changes and signals.
* @param id The callback event identifier.
* @param idxTrigger The trigger index if defined (-1 is not defined).
* @param temperature  The temperature triggering the event.
*/
forward app_TemperatureSensorEvent(  
	idTscb,
	idxTrigger,
	temperature
);