# Cora Asset Tracker Application Release Notes

### V1.028a - 231208
1. Fixed CPHUB configuration issue with v1.027a, otherwise identical

### V1.027a - 231206
1. Updated CT1XXX firmware reference to v2.4.4.2a
 - Fixed TimeSync bug (KP-350)
 - Various RTC Kernel Fixes
 - NETWORK RESET operation now rejoins instead of clearing NVM params
 - DUX12 accelerometer bug fixes
2. LoraWAN v1.04 radio stack
  - US915 SB2 / ADR Disabled
  - EU868 Standard Band / ADR enabled

### V1.026a_104 - 230920
1. Firmware Reference v2.4.0.0
2. Firmware transitions to v1.04 LoRaWAN compliant radio library

### V1.025 - 230920
1. Firmware Reference v2.3.0.0 CT100x / v2.3.0.1 CT1020
2. Fixed debug support and messaging for ImpulseSensor

### V1.024 - 230919
1. Firmware Reference v2.3.0.0
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.

### 1.023a - 230831
1. Firmware Reference v2.2.3.0k
   - DUX12 fixes for ImpulseSensor

### 1.022a - 230818
1. Firmware Reference v2.2.2.2k
   - Power fixes.  See firmware release notes for details.

### 1.021 - 230708
1. Firmware Reference v2.2.1.0
 - Production release of KP_STABLE1

### 1.020a - 230708
1. Firmware Reference v2.2.0.3k KP_STABLE1
 - Cleanup BVT / Hibernate behavior
 
### 1.019a - 230706
1. Firmware Reference v2.2.0.2k KP_STABLE1
 - Many fixes, see firmware notes
 - Uses OPL v1.0.0.4 requires new N100 and Production tools to be compatible
 - Changed Impulse module to not log WoM messages

### 1.018a - 230628
1. Firmware Reference v2.2.0.1k KP_STABLE1
 - Foundation Bug fixes: Callback subsystem

### 1.017a - 230616
1. Firmware Reference v2.2.0.0k KP_STABLE1
 - Foundation Bug fixes: Queue, Callback subsystems
 - Added TimerTick API to PAWN-API

### 1.016a - 230616
1. Firmware Reference v2.1.5.2k KP_STABLE1
 - Reverts OPL build to v1.0.0.2a to maintain tool compatibility
 - Enables BIST for OPL testing
 - Limited OnCharger log message to 1 hour each

### 1.015a - 230616
  1. Firmware Reference v2.1.5.1k KP_STABLE1
  - New OPL Version 1.0.0.3 fixes USER reset issue
* Build 1.014a 230614
1. Identical to v1.013a except addec CT1020 bundle using same KP_STABLE1 build

### 1.013a - 230614
1. Firmware reference v2.1.5.0k KP_STABLE1
   * Fixes RTC Alarm callback issue (KP-264)
2. Disabled TELEM_DEBUG on alpha builds

### 1.012a - 230613
1. Firmware reference v2.1.4.2k KP_STABLE1
2. Added RTC Callback debug/recovery to Motion events in impulsesensor.c

### 1.011a - 230609
1. Firmware reference v2.1.4.1k KP_STABLE1
    * Fixes SysLog issues opon new app download
	* 30 minute BIST statistics interval
2. Released 1.011a_LI variant
	* Changed Impulse threshold/exit to 300mG / 100mG

### 1.010a - 230606
1. Firmware reference v2.1.4.0k KP_STABLE1
    * Enables BIST functions
	* Reverts to standard LoRaWAN NVM behavior
	* does not support Rev4 (DUX12) hardware with BIST functions
2. Added TELEMDEBUG option for syslog entries with CRITICAL priority to be uplinked

### 1.006a - 230602
1. Firmware reference v2.1.3.1a
	* Disables LoraWan NVM update (testing only)
2. Added support for rev-4 hardware

### 1.005a - Debug Build / Contractor debug
1. Added debug code testing only build

### 1.004a - 230517
1. Firmware Reference v2.1.2.1a --
	 Reworked Platform Positioning acquisition and complete code in attempt to eliminate cases where it does not transition to IDLE after failure.
	 - Restored timers to Kernel_Timer implementation (STM will be on entire time positioning is happening).
	 - Removed comm communication failure cases.
	 - Changed state update to eliminate potential re-entrancy. 
	 - Added support for hardware Rev4 CT1000 configurations
### 1.003a, 230508
1. Firmware reference v2.1.1.2a
   * Changed HAL pre-read / pre-fetch configuration.
2. Set default impulse threshold to 2200 mGs.
3. Set default WOM threshold to  80 mGs.
4. Set default tracking interval to 2 minutes.
5. Baseline release to consolidate known hardware issues to begin 
   comprehensive testing.

### 1.002, 230502 (DEPRECATED - Had Problems)
1. Firmware reference v2.1.1.1a
   * Changed I2C memory bus from 100Khz to 400Khz.
   * Fixed accelerometer and i2c memory bugs.
   * Changed BVT LED behavior (manufacturing).
2. Platform version v1.3.1.4
3. Changed default positioning mode to default (coarse).


### 1.001a, 230425
1. Initial Release
2. Firmware reference v2.1.0.1a
3. Platform version v1.3.1.4





---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
