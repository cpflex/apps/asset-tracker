@ECHO OFF
if [%1]==[] goto usage

echo ** Compiling /  Publishing Alpha Release Version Application and Marking as Latest.
call compile %1
call cphub_push_alpha %1

echo ** Compiling /  Publishing Alpha Release Version Application with SysLog Enabled 
call compile %1_syslog  SYSLOG
call cphub_push_alpha %1_syslog

REM echo ** Compiling /  Publishing Alpha Release Version Application with SysLog and Telem Uplink Enabled 
REM call compile %1_telemdebug  SYSLOG TELEMDEBUG
REM call cphub_push_alpha %1_telemdebug

REM echo ** Compiling /  Publishing Alpha Test Version Application with SysLog Enabled 
REM call compile %1_test_syslog  TEST SYSLOG
REM call cphub_push_alpha %1_test_syslog

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
